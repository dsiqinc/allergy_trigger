set TRIGGERING_DAY = 25;
set TRIGGERING_DAYOFWEEK = Wed;
set TRIGGER_THRESHOLD = 3.0;

-----------------------------------------------------------------------
---Enter Triggering Date and Day of Week Above (Usually Yesterday)
---Possible Day of Week: Mon, Tue, Wed, Thu, Fri, Sat, Sun
---The most recent subcat index
---Trigger threshold should be parameterized
-----------------------------------------------------------------------

-------------------Update subcatindex table with new data if needed-------------
use salesindex;
alter table subcatindex add IF NOT EXISTS partition (year=${hiveconf:TRIGGERING_YEAR},month=${hiveconf:TRIGGERING_MONTH},day=${hiveconf:TRIGGERING_DAY}) location

'/asset/wm/subcategory/index/${hiveconf:TRIGGERING_YEAR}/${hiveconf:TRIGGERING_MONTH}/${hiveconf:TRIGGERING_DAY}';
----------------------------------------------------------------------

------------ Determine which stores will trigger -----------------------
--- A list of the triggered stores will be in '/user/pcalderhead.adm/SalesTrigger/triggered_stores'
use salestrigger;
insert overwrite table external_triggered_stores
select a.storenumber
from salesindex.subcatindex a
join salestrigger.allergy_baseline_table b
on a.storenumber = b.storenumber
where b.DayOfWeek = '${hiveconf:TRIGGERING_DAYOFWEEK}'
and a.subcatIndex/b.AvgSubcatIndex > ${hiveconf:TRIGGER_THRESHOLD}
and a.year = ${hiveconf:TRIGGERING_YEAR}
and a.month = ${hiveconf:TRIGGERING_MONTH}
and a.day = ${hiveconf:TRIGGERING_DAY}
and a.subcat in (
'3505',
'19860',
'19861',
'19862',
'19863',
'28170')
group by a.storenumber;
