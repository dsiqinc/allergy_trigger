use salestrigger;

---------------------------------------------------------------------
---------------- list triggered stores ------------------------------
-- Run by the Run-AllergyTriggers.sh script after the data has been collected --

select storenumber, date_add(FROM_UNIXTIME( UNIX_TIMESTAMP() ),1), date_add(FROM_UNIXTIME( UNIX_TIMESTAMP() ),1) from external_triggered_stores order by storenumber;

