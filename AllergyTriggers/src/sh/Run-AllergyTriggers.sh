#!/bin/bash
# Filename: Run-AllergyTriggers.sh
# Description: This script will query a Hive table to determine
# the number of stores that triggered on allergy
# Change History: 03/10/15, Kevin Verhoeven, Initial Creation
#
#ScriptsFolder=$1
ScriptsFolder=/opt/ds-iq/AllergyTriggers
# output folder where RDS is set to pull from
#OutputFolder=$2
OutputFolder=/home/rdsazuprodapp01/rds/AllergyTriggers
#OutputFileTriggeredStores=$3
OutputFileTriggeredStores="AllergyTriggers_TriggeredStores"
#OutputFileStoreCount=$4
OutputFileStoreCount="AllergyTriggers_StoreCount"
#WorkingFolder=$5
WorkingFolder="work";
#LogFolder=$6
LogFolder=/var/log/ds-iq
#DateTime=$7
DateTime=$(date +%Y%m%d%H%M%S)

# setup logging
LogFolderFile="$LogFolder/AllergyTriggers.$DateTime.log";
echo $DateTime > "$LogFolderFile";

# Run AllergyTriggers.hql to add metadata to the Hive
# table if not exist (adding in the new fineline date folders)
hive -f "$ScriptsFolder/AllergyTriggers-Configuration.hql" >> "$LogFolderFile" 2>&1;

LogResults=`cat "$LogFolderFile"|tail -n2|head -n1`;
echo "LogResults:"$LogResults;
# test results, pass if OK, otherwise fail
if [ ! "$LogResults" == "OK" ]; then
        echo "Error! Script: AllergyTriggers.hql failed.";
        echo "ERROR: Hive script failed to apply metadata! Script: AllergyTriggers.hql" >> "$LogFolderFile";
        shopt -u nullglob
        exit
fi

#An example of pulling a value using Bash looks something like this:
#my_value=`hive -S -hiveconf MY_VAR1=value1 -hiveconf MY_VAR2=value2 -f my_script.hql`

TriggerResults=`hive -S -f "$ScriptsFolder/AllergyTriggers-StoreCount.hql"|tail -n1` >> "$LogFolderFile" 2>&1;
echo "TriggerResults:"$TriggerResults;
echo $TriggerResults > "$OutputFolder/$WorkingFolder/$OutputFileStoreCount.$DateTime.txt";

# test results, pass if a number, otherwise fail
if ! [[ $TriggerResults =~ ^[0-9]+$ ]]; then
        echo "Error! The store count was not numeric!";
        echo "ERROR: Hive script failed, the results were not numeric!" >> "$LogFolderFile";
        shopt -u nullglob
        exit
fi

# get list of triggered stores
hive -S -f "$ScriptsFolder/AllergyTriggers-TriggeredStores.hql" > "$OutputFolder/$WorkingFolder/$OutputFileTriggeredStores.$DateTime.txt" 2>&1;
# alternatively you can load the results into a variable, but I noticed that the line breaks were stripped
#TriggeredStoreResults=`hive -S -f "$ScriptsFolder/AllergyTriggers-TriggeredStores.hql"|tail -n$TriggerResults` >> "$LogFolderFile" 2>&1;
#echo $TriggeredStoreResults > "$OutputFolder/$WorkingFolder/$OutputFileTriggeredStores.$DateTime.txt";

# remove the header and fix line breaks
sed -i -e "1d" "$OutputFolder/$WorkingFolder/$OutputFileTriggeredStores.$DateTime.txt";
sed -i 's/$/\r/' "$OutputFolder/$WorkingFolder/$OutputFileTriggeredStores.$DateTime.txt";

# move successful results file into the output folder for RDS to get:
mv "$OutputFolder/$WorkingFolder/$OutputFileStoreCount.$DateTime.txt" "$OutputFolder/$OutputFileStoreCount.$DateTime.txt";
mv "$OutputFolder/$WorkingFolder/$OutputFileTriggeredStores.$DateTime.txt" "$OutputFolder/$OutputFileTriggeredStores.$DateTime.txt";

