use salestrigger;

---------------------------------------------------------------------
---------------- Determine how many stores would trigger ------------
-- Run by the Run-AllergyTriggers.sh script after the data has been collected --

select count(*) from external_triggered_stores;

